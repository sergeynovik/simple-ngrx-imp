import { User } from './../../interfaces/users.interface';
import { createAction, props } from '@ngrx/store';

export enum UsersActions {
  GetUsers = '[USERS] Get',
  GetUsersSuccess = '[USERS] Get success',
  GetUsersFailed = '[USERS] Get failed',
  ClearUsers = '[USERS] Clear',
}

export const getusers = createAction(
  UsersActions.GetUsers,
  props<{ count: number }>()
);
export const getUsersSuccess = createAction(
  UsersActions.GetUsersSuccess,
  props<{ users: User[] }>()
);
export const getUsersFailed = createAction(
  UsersActions.GetUsersFailed,
  props<{ error: string }>()
);
export const clearUsers = createAction(UsersActions.ClearUsers);
