import { ActionReducerMap } from '@ngrx/store';
import * as usersReducer from './reducers/users.reducer';

export interface State {
  users: usersReducer.UsersState;
}

export const reducers: ActionReducerMap<State> = {
  users: usersReducer.reducer,
};
