import {
  getUsersSuccess,
  getUsersFailed,
  clearUsers,
} from './../actions/users.action';
import { Action, createReducer, on } from '@ngrx/store';
import { getusers } from '../actions/users.action';
import { User } from './../../interfaces/users.interface';

export interface UsersState {
  usersArray: User[];
  error: string | null;
  isLoaidng: boolean;
}

const initialState: UsersState = {
  usersArray: [],
  error: null,
  isLoaidng: false,
};

export const reducer = createReducer(
  initialState,
  on(getusers, (state) => ({
    ...state,
    isLoaidng: true,
  })),
  on(getUsersSuccess, (state, { users }) => ({
    ...state,
    usersArray: [...state.usersArray, ...users],
    error: null,
    isLoaidng: false,
  })),
  on(getUsersFailed, (state, { error }) => ({
    ...state,
    error,
    usersArray: [],
    isLoaidng: false,
  })),
  on(clearUsers, () => ({
    ...initialState,
  }))
);
