import { createSelector } from '@ngrx/store';
import { State } from '..';
import { UsersState } from '../reducers/users.reducer';

export const selectUsersFeature = (state: State) => state.users;

export const isUsersLoadng = createSelector(
  selectUsersFeature,
  (state: UsersState) => state.isLoaidng
);

export const selectUsers = createSelector(
  selectUsersFeature,
  (state: UsersState) => state.usersArray
);

export const selectUsersCount = createSelector(
  selectUsersFeature,
  (state: UsersState) => (state.usersArray ? state.usersArray.length : 0)
);

export const selectError = createSelector(
  selectUsersFeature,
  (state: UsersState) => state.error
);
