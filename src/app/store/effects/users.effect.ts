import { Action } from '@ngrx/store';
import { UsersService } from './../../services/users.service';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  UsersActions,
  getUsersSuccess,
  getUsersFailed,
} from './../actions/users.action';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class UsersEffects {
  constructor(private actions: Actions, private usersService: UsersService) {}

  private getUsers$: Observable<Action> = createEffect(() =>
    this.actions.pipe(
      ofType(UsersActions.GetUsers),
      mergeMap(({ count }) =>
        this.usersService.getUsers(count).pipe(
          map((users) => getUsersSuccess({ users })),
          catchError((err: HttpErrorResponse) =>
            of(getUsersFailed({ error: err.message }))
          )
        )
      )
    )
  );
}
