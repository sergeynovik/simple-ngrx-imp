import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User, UsersAPI } from '../interfaces/users.interface';

@Injectable({ providedIn: 'root' })
export class UsersService {
  constructor(private httpClient: HttpClient) {}

  public getUsers(count = 10): Observable<User[]> {
    return this.httpClient
      .get<UsersAPI>(`https://randomuser.me/api/?results=${count}`)
      .pipe(map((usersApi) => usersApi.results));
  }
}
