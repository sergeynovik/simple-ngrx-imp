import { clearUsers, getusers } from './../store/actions/users.action';
import { UsersService } from '../services/users.service';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import {
  isUsersLoadng,
  selectUsers,
  selectUsersCount,
  selectError,
} from '../store/selectors/users.selector';

@Component({
  selector: 'app-ngrx-page',
  templateUrl: './ngrx-page.component.html',
  styleUrls: ['./ngrx-page.component.scss'],
})
export class NgrxPageComponent implements OnInit {
  constructor(private store: Store) {}

  public isUsersLoadng = this.store.pipe(select(isUsersLoadng));
  public users = this.store.pipe(select(selectUsers));
  public usersCount = this.store.pipe(select(selectUsersCount));
  public usersLoadngError = this.store.pipe(select(selectError));

  ngOnInit(): void {}

  public getUsers(): void {
    this.store.dispatch(
      getusers({
        count: 10,
      })
    );
  }

  public clearUsers(): void {
    this.store.dispatch(clearUsers());
  }
}
