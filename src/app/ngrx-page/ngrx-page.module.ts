import { NgrxPageRoutingModule } from './ngrx-page-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgrxPageComponent } from './ngrx-page.component';

@NgModule({
  declarations: [NgrxPageComponent],
  imports: [NgrxPageRoutingModule, CommonModule],
})
export class NgrxPageModule {}
